import java.io.*;
import java.util.regex.Pattern;
import java.util.Random;
public class MergeSort{
    private static int counter = 0;
    private static final Pattern space = Pattern.compile(" ");
    public static void main(String[]args)throws IOException{ 
        //control
        //newList being true will cause a new list to be generated on run time
        boolean newList = true;
        //each boolean below controls which algorithm is being tested
        boolean a = false;
        boolean b = true;
        boolean c = false;
        boolean d = false;
        if(newList) nInput();
        double [] nums = getNums();
        int num_elements = getTotal();
        String thing = "";
        //I made these booleans to easily switch between which alorithm is being tested.
        if(a){
            MergeSortA(nums, 0, num_elements-1);
            for(int i = 0; i<num_elements; i++){
                thing = thing + nums[i];
                if(i<num_elements-1) thing = thing + " ";
            }
        }
        if(b){
            MergeSortBSetup(nums, num_elements);
            for(int i = 0; i<num_elements; i++){
                thing = thing + nums[i];
                if(i<num_elements-1) thing = thing + " ";
            }
        }
        if(c){
            MergeSortC(nums, num_elements);
            for(int i = 0; i<num_elements; i++){
                thing = thing + nums[i];
                if(i<num_elements-1) thing = thing + " ";
            }
        }
        if(d){
            MergeSortD(nums, num_elements);
            for(int i = 0; i<num_elements; i++){
                thing = thing + nums[i];
                if(i<num_elements-1) thing = thing + " ";
            }
        }
        writeFile(thing);
        System.out.println( counter + "");
    }
    public static void MergeSortA(double [] nums, int left, int right){
        int mid;
        if(right>left){
            mid = (right +left)/2;
            MergeSortA(nums, left, mid);
            MergeSortA(nums, mid+1, right);
            Merge(nums, left, right, mid+1);
            counter++;
        }
    }
    public static void MergeSortBSetup(double [] nums, int num_elements){
        int curr_size;
        counter++;
        int left_start;
        counter++;
        for(curr_size= 1; curr_size <= num_elements-1; curr_size = 2*curr_size){
            counter++;
            for(left_start = 0; left_start < num_elements-1; left_start += 2*curr_size){
                counter++;
                int mid = Math.min(left_start + curr_size - 1, num_elements-1);
                counter++;
                int right_end = Math.min(left_start + 2*curr_size -1, num_elements-1);
                counter++;
                MergeSortB(nums, left_start, mid, right_end);
            }
        }
    }
    public static void MergeSortB(double [] nums, int left, int mid, int right){
        int i, j, k;
        counter++;
        counter++;
        counter++;
        int n1 = mid - left + 1;
        counter++;
        int n2 = right - mid;
        counter++;
        double[] temp1 = new double[n1];
        counter++;
        double[] temp2 = new double[n2];
        counter++;
        for(i=0; i < n1; i++){
            temp1[i] = nums[left + i];
            counter++;
        }
        for(j=0; j<n2; j++){
            temp2[j] = nums[mid + 1 +j];
            counter++;
        }
        i= 0;
        counter++;
        j = 0;
        counter++;
        k = left;
        counter++;
        while(i< n1 && j< n2){
            counter++;
            if(temp1[i] <= temp2[j]){
                counter++;
                nums[k] = temp1[i];
                counter++;
                i++;
            }
            else{
                counter++;
                nums[k] = temp2[j];
                counter++;
                j++;
            }
            k++;
        }
        while(i<n1){
            counter++;
            nums[k] = temp1[i];
            counter++;
            i++;
            counter++;
            k++;
            counter++;
        }
        while(j<n2){
            nums[k] = temp2[j];
            counter++;
            j++;
            counter++;
            k++;
            counter++;
        }
    }
    public static void MergeSortC(double [] nums, int num_elements){
        int left = 0;
        counter++;
        int right;
        counter++;
        double temp;
        counter++;
        int temp2;
        counter++;
        int LIMIT = 25;
        counter++;
        if (num_elements <= LIMIT){
            counter++;
            right = 1;
            counter++;
            while(right < num_elements){
                counter++;
                temp = nums[right];
                counter++;
                while(( left > (-1) ) && ( nums[left] > temp )){
                    counter++;
                    nums[left+1] = nums[left--];
                    counter++;
                }
                nums[left+1] = temp;
                counter++;
                left = right;
                counter++;
                right++;
                counter++;
            }
        }
        else{
            //Parts of this method came from:
            //https://stackoverflow.com/questions/1557894/non-recursive-merge-sort
            int i, j;
            counter++;
            counter++;
            right = LIMIT;
            counter++;
            while (right <= num_elements){
                counter++;
                i = left + 1;
                counter++;
                j = left;
                counter++;
                while (i < right){
                    counter++;
                    temp = nums[i];
                    counter++;
                    while(( j >= left ) && ( nums[j] > temp )){
                        counter++;
                        nums[j+1] = nums[j--];
                        counter++;
                    }
                    nums[j+1] = temp;
                    counter++;
                    j = i;
                    counter++;
                    i++;
                    counter++;
                }
                left = right;
                counter++;
                right = right + LIMIT;
                counter++;
            }
            i = left + 1;
            counter++;
            j = left;
            counter++;
            while(i < num_elements){
                counter++;
                temp = nums[i];
                counter++;
                while(( j >= left ) && ( nums[j] > temp )){
                    counter++;
                    nums[j+1] = nums[j--];
                    counter++;
                }
                nums[j+1] = temp;
                counter++;
                j = i;
                counter++;
                i++;
                counter++;
            }
            right = num_elements -1;
            counter++;
            left = 0;
            counter++;
            int mid = (right +left)/2;
            counter++;
            MergeSortA(nums, left, mid);
            MergeSortA(nums, mid+1, right);
            Merge(nums, left, right, mid+1);
            
        }
    }
    public static void MergeSortD(double [] nums, int num_elements){    
        int left = 0;
        counter++;
        int right;
        counter++;
        double temp;
        counter++;
        int temp2;
        counter++;
        int LIMIT = 25;
        counter++;
        if (num_elements <= LIMIT){
            counter++;
            right = 1;
            counter++;
            while(right < num_elements){
                counter++;
                temp = nums[right];
                counter++;
                while(( left > (-1) ) && ( nums[left] > temp )){
                    counter++;
                    nums[left+1] = nums[left--];
                    counter++;
                }
                nums[left+1] = temp;
                counter++;
                left = right;
                counter++;
                right++;
                counter++;
            }
        }
        else{
            counter++;
            //Parts of this method came from:
            //https://stackoverflow.com/questions/1557894/non-recursive-merge-sort
            int i, j;
            counter++;
            counter++;
            right = LIMIT;
            counter++;
            while (right <= num_elements){
                counter++;
                i = left + 1;
                counter++;
                j = left;
                counter++;
                while (i < right){
                    counter++;
                    temp = nums[i];
                    counter++;
                    while(( j >= left ) && ( nums[j] > temp )){
                        counter++;
                        nums[j+1] = nums[j--];
                        counter++;
                    }
                    nums[j+1] = temp;
                    counter++;
                    j = i;
                    counter++;
                    i++;
                    counter++;
                }
                left = right;
                counter++;
                right = right + LIMIT;
                counter++;
            }
            i = left + 1;
            counter++;
            j = left;
            counter++;
            while(i < num_elements){
                counter++;
                temp = nums[i];
                counter++;
                while(( j >= left ) && ( nums[j] > temp )){
                    counter++;
                    nums[j+1] = nums[j--];
                    counter++;
                }
                nums[j+1] = temp;
                counter++;
                j = i;
                counter++;
                i++;
                counter++;
            }
            double[] temp_array = new double[num_elements];
            counter++;
            double[] swap;
            counter++;
            int k = LIMIT;
            counter++;
            while (k < num_elements){
                counter++;
                left = 0;
                counter++;
                i = k;
                counter++;
                right = k << 1;
                counter++;
                while (i < num_elements){
                    counter++;
                    if (right > num_elements){
                        counter++;
                        right = num_elements;
                        counter++;
                    }
                    temp2 = left;
                    counter++;
                    j = i;
                    counter++;
                    while ((left < i) && (j < right)){
                        counter++;
                        if (nums[left] <= nums[j]){
                            counter++;
                            temp_array[temp2++] = nums[left++];
                            counter++;
                        }
                        else{
                            counter++;
                            temp_array[temp2++] = nums[j++];
                            counter++;
                        }
                    }
                    while (left < i){
                        counter++;
                        temp_array[temp2++] = nums[left++];
                        counter++;
                    }
                    while (j < right){
                        counter++;
                        temp_array[temp2++] = nums[j++];
                        counter++;
                    }
                    left = right;
                    counter++;
                    i = left + k;
                    counter++;
                    right = i + k;
                    counter++;
                }
                while (left < num_elements){
                    counter++;
                    temp_array[left] = nums[left++];
                    counter++;
                }
                swap = nums;
                counter++;
                nums = temp_array;
                counter++;
                temp_array = swap;
                counter++;
                k <<= 1;
                counter++;
            }
        }
    }
    public static void Merge(double[] nums, int left, int right, int mid){
        double [] temp = new double[3000000];
        counter++;
        int left_pos = 0;
        counter++;
        int temp_pos = 0;
        counter++;
        left_pos = mid-1;
        counter++;
        temp_pos = left;
        counter++;
        int num_elements = right-left+1;
        counter++;
        while((mid <= right) && (left <= left_pos)){
            if(nums[left] <= nums[mid]){
                counter++;
                temp[temp_pos++] = nums[left++];
                counter++;
            }
            else{
                counter++;
                temp[temp_pos++] = nums[mid++];
                counter++;
            }  
            counter++;
        }
        while(left <= left_pos){
            counter++;
            temp[temp_pos++] = nums[left++];
            counter++;
        }
        while(mid <= right){
            counter++;
            temp[temp_pos++] = nums[mid++];
            counter++;
        }
        for(int i=0; i < num_elements; i++){
            counter++;
            nums[right] = temp[right];
            counter++;
            right--;
            counter++;
        }
    }
    public static double[] getNums()throws IOException{
        double [] nums = null;
        try{
            BufferedReader in = new BufferedReader(new FileReader("inputHW02.txt"));
            nums = new double[3000000];
            int num_elements = 0;
            String line;
            while((line = in.readLine()) != null){
                for(String token: space.split(line)){
                    try{nums[num_elements] = Double.parseDouble(token);
                        num_elements++;
                    }
                    catch(NumberFormatException ex){
                        System.err.println(token + " is not a number");
                    }
                }
            }
            in.close();
        }
        catch(FileNotFoundException e){
        }
        return nums;
    } 
    public static int getTotal()throws IOException{
        double [] nums = null;
        int total = 0;
        try{
            BufferedReader in = new BufferedReader(new FileReader("inputHW02.txt"));
            nums = new double[3000000];
            int num_elements = 0;
            String line;
            while((line = in.readLine()) != null){
                for(String token: space.split(line)){
                    try{ total++;
                    }
                    catch(NumberFormatException ex){
                        System.err.println(token + " is not a number");
                    }
                }
            }
            in.close();
        }
        catch(FileNotFoundException e){
        }
        return total;
    } 
    public static void writeFile(String thing) throws IOException{
        FileWriter out = new FileWriter("outputHW02.txt", false);
        out.write(thing);
        out.close();
    }
    public static void populateInput() throws IOException{
        FileWriter out = new FileWriter("inputHW02.txt", false);
        Random r = new Random();
        //I'm lowering max numbers to 300 for this so it doesnt take an incredibly long time to run
        int n = r.nextInt(300);
        for(int i = 0; i < n; i++){
            out.write((Math.round(r.nextDouble()*1000000))/100.0 + "");
            if(i<n-1) out.write(" ");
        }
        out.close();
    }
    public static void nInput() throws IOException{
        FileWriter out = new FileWriter("inputHW02.txt", false);
        Random r = new Random();
        //I'm lowering max numbers to 300 for this so it doesnt take an incredibly long time to run
        int n = 3000000;
        for(int i = 0; i < n; i++){
            out.write((Math.round(r.nextDouble()*1000000))/100.0 + "");
            if(i<n-1) out.write(" ");
        }
        out.close();
    }
}
