// Linked lists
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Test {
    public static void main(String[]args){
        NameList names = new NameList();
        names.append("Smith");
        names.append("Jones");
        names.append("White");
        names.print();
        System.out.println();   
        names.remove("Smith");
        names.print();
        //names.insert("Jones", "Schmit");
    }
}