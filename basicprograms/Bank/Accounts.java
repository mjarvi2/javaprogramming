
public class Accounts{
    private BankAccountNode accounts;
    public Accounts(){
        accounts = null;
    }
    public boolean isEmpty(){
        return accounts == null;
    }
    public boolean hasNext(){
        return accounts.getNext()!= null;
    }   
    public void append(BankAccount acct){
        BankAccountNode temp = accounts;
        if(isEmpty()){
            accounts = new BankAccountNode(acct, null);
        }   
        else{
            while(temp.getNext()!= null){
                temp = temp.getNext();
            }
            temp.setNext(new BankAccountNode(acct, null));
        }
    }
    public void remove(String name){
        boolean removeAccount = true;
        BankAccountNode temp1 = accounts;
        BankAccountNode temp2 = accounts;
        while(!temp1.getAccount().getName().equals(name)){
             temp2 = temp1;
             temp1 = temp1.getNext();
             removeAccount = false;
        }
        if(removeAccount){
            accounts = accounts.getNext();
        }
        else{
            temp2.setNext(temp1.getNext());
        }
    }
    public void changePassword(String username, String oldPassword, String newPassword){
        BankAccountNode temp = accounts;
        while(!temp.getAccount().getUsername().equals(username)){
            temp = temp.getNext();
        }
        if(temp.getAccount().getPassword().equals(oldPassword)){
            temp.getAccount().setPassword(newPassword);
        }
    }
    public void print(){
        BankAccountNode temp = accounts;
        while(temp!= null){
            System.out.println("Name: " + temp.getAccount().getName() + " Username: " +
                temp.getAccount().getUsername() + " Password: " + 
                    temp.getAccount().getPassword());
            temp = temp.getNext();
        }
        System.out.println();
    }
}
