
public class BankAccountNode{
    private BankAccount account;
    private BankAccountNode next;
    public BankAccountNode(BankAccount account, BankAccountNode next){
        this.account = account;
        this.next = next;
    }
    public BankAccount getAccount(){
        return account;
    }
    public void setAccount(BankAccount account){
        this.account = account;
    }
    public BankAccountNode getNext(){
        return next;
    }   
    public void setNext(BankAccountNode next){
        this.next = next;
    }
}
