// Linked lists
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class NameNode {
    private String lName;
    private NameNode next;
    public NameNode(String name,NameNode next){
        lName = name;
        this.next = next;
    }
    public String getName(){ return lName;}
    public void setName(String name){ this.lName = name;}
    public NameNode getNext(){ return next;}
    public void setNext(NameNode next){ this.next = next;}
}
