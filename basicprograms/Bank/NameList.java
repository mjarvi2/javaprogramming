// Linked lists
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class NameList {
    private NameNode head;
    public NameList(){
        head = null;
    }
    public boolean isEmpty(){ return head == null;}
    public void append(String name){
        NameNode temp = head;
        if(isEmpty()){
            head = new NameNode(name, null);
        }
        else{
           while(temp.getNext() != null){
               temp = temp.getNext();
           }
           temp.setNext(new NameNode(name, null)); 
        }
    }
    public void print(){ 
        NameNode temp = head;
        while(temp != null){
            System.out.println(temp.getName()); 
            temp = temp.getNext();
        }
    }
    public void insert(String newName, String beforeName){
     boolean insertHead = true;
     NameNode temp1 = head;
     NameNode temp2 = null;
     while(temp1.getName() != beforeName){
       temp2 = temp1;
       temp1 = temp1.getNext();
       insertHead = false;
     }
      if(insertHead) head = new NameNode(newName, temp2);
      else
        temp2.setNext( new NameNode(newName, temp1));
    }
    public void remove(String beforeName){
        boolean deleteHead = true;
     NameNode temp1 = head;
     NameNode temp2 = head;
     while(temp1.getName() != beforeName){
       temp2 = temp1;
       temp1 = temp1.getNext();
       deleteHead = false;
     }
      if(deleteHead) head = head.getNext();
      else
        temp2.setNext(temp1.getNext());
   }
}