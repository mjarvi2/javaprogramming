package src.main.java;

// Student class
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Student extends Person{
    int earned_credits;
    double gpa;
    String major;
    public Student(int ec, double gpa, String major, String name, 
            int age, HeightAndWeight height_weight){
        super(name, age, height_weight);
        earned_credits = ec;
        this.gpa = gpa;
        this.major = major;
    }
    public int getCredits(){
        return earned_credits;
    }
    public double getGpa(){
        return gpa;
    }
    public String getMajor(){
        return major;
    }
    private String getStatus(){
        if(earned_credits<=30 && earned_credits >=0) return "Freshman";
        else if(earned_credits>30 && earned_credits <=60) return "Sophomore";
        else if(earned_credits>60 && earned_credits <=90) return "Junior";
        return "Senior";
    }
    private boolean onDeansList(){
        return gpa>=3.4;
    }
    public String toString(){
        return(super.toString() + " " + getName() + " is a " + getStatus() +
                " and a " + getMajor() + " major and it is " + 
                onDeansList() + " that they are on the deans list.");
    }
}
