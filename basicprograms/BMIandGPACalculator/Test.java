package src.main.java;

public class Test{
    public static void main(String[]args){
        //HeightAndWeight person1 = ;
        HeightAndWeight person1_metric = new HeightAndWeight(70.2,160.3).convertToMetric();
        Student person1_moreInfo = new Student(60, 3.6, "Computer Science", "Eugene", 19, person1_metric);
        System.out.println(person1_moreInfo);
        HeightAndWeight person2_metric = new HeightAndWeight(65.7,157.6).convertToMetric();
        Student person2_moreInfo = new Student(30, 3.4, "Psychology", "Charles", 18, person2_metric);
        System.out.println(person2_moreInfo);
        HeightAndWeight person3_metric = new HeightAndWeight(69.8,170.2).convertToMetric();
        Student person3_moreInfo = new Student(57, 3.2, "Music", "Amanda", 19, person3_metric);
        System.out.println(person3_moreInfo);
        HeightAndWeight person4_metric = new HeightAndWeight(65.7,160.3).convertToMetric();
        Student person4_moreInfo = new Student(91, 4.0, "Early Education", "Andrew", 21, person4_metric);
        System.out.println(person4_moreInfo);
        HeightAndWeight person5_metric = new HeightAndWeight(74.2,180.2).convertToMetric();
        Student person5_moreInfo = new Student(90, 3.5, "Physics", "Shaun", 20, person5_metric);
        System.out.println(person5_moreInfo);
    }
}
/* TEST
Eugene, who is 19 years old, has a BMI of 22.0 and it is false that Eugene is overweight. Eugene is a Sophomore and a Computer Science major and it is true that they are on the deans list.
Charles, who is 18 years old, has a BMI of 25.0 and it is true that Charles is overweight. Charles is a Freshman and a Psychology major and it is true that they are on the deans list.
Amanda, who is 19 years old, has a BMI of 24.0 and it is false that Amanda is overweight. Amanda is a Sophomore and a Music major and it is false that they are on the deans list.
Andrew, who is 21 years old, has a BMI of 26.0 and it is true that Andrew is overweight. Andrew is a Senior and a Early Education major and it is true that they are on the deans list.
Shaun, who is 20 years old, has a BMI of 23.0 and it is false that Shaun is overweight. Shaun is a Junior and a Physics major and it is true that they are on the deans list.
*/