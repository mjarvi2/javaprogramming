// HeightAndWeight class
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class HeightAndWeight {
    private double height, weight;
    public HeightAndWeight(double height, double weight){
        //alternate constructor
        this.height = height;
        this.weight = weight;
    }
    public HeightAndWeight convertToMetric(){
        return new HeightAndWeight(convertToMeters(height), convertToKgs(weight));
    }
    public double getHeight(){
        //return height
        return height;
    }
    public double getWeight(){
        //return weight
        return weight;
    }
    public double BMI(){
        //get BMI number and return it using kgs and meters
        double bmi = weight/(Math.pow(height,2));
        return bmi;
    }
    public boolean healthyBMI(){
        // using BMI method, figure out if BMI is healthy(true or false)
        if(BMI()>= 18.5 && BMI() <= 24.9)
            return true;
        return false;
    }
    public String getDescript(){
        //figure out is the person's weight is healthy using various body types
        String description ="";
        if(BMI()<18.5) return "Underweight";
        else if(BMI()>= 18.5 && BMI() <= 24.9) description = "Normal";
        else if(BMI()>24.9 && BMI()<=29.9) description =  "Overweight";
        else if(BMI()>29.9) description = "Obese";
        return description;   
    }
    public String toString(){
        //override toString method to give results
        return "Your body mass index is: " +(double)(Math.round(BMI()*100))/100 +
        " kg/m^2. \n" + "It is " +healthyBMI() + " that you have a normal BMI.\n"+
        "Your body type is " + getDescript() + "\n";
    }
    private double convertToMeters(double inches){
        //helper method for BMI to give meters from inches
        return inches / 39.3701;
    }
    private double convertToKgs(double pounds){
        //Helper method for BMI to give kgs from pounds
        return pounds / 2.205;
    }
}


