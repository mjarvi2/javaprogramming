//Michael Jarvis
//Test Person class
public class Test{
    public static void main(String[]args){
        HeightAndWeight person1 = new HeightAndWeight(70.2,160.3);
        HeightAndWeight person1_metric = person1.convertToMetric();
        Person person1_moreInfo = new Person("Eugene", 22, person1_metric);
        HeightAndWeight person2 = new HeightAndWeight(50,180);
        HeightAndWeight person2_metric = person2.convertToMetric();
        Person person2_moreInfo = new Person("Hubert", 19, person2_metric);
        HeightAndWeight person3 = new HeightAndWeight(70.3,126);
        HeightAndWeight person3_metric = person3.convertToMetric();
        Person person3_moreInfo = new Person("Abby", 23, person3_metric);
        HeightAndWeight person4 = new HeightAndWeight(67, 162.4);
        HeightAndWeight person4_metric = person4.convertToMetric();
        Person person4_moreInfo = new Person("Eduardo", 35, person4_metric);

        System.out.println(person1_moreInfo);
        System.out.println(person2_moreInfo);
        System.out.println(person3_moreInfo);
        System.out.println(person4_moreInfo);
        
        /** TEST
         * Eugene, who is 22 years old, has a BMI of 22.0 and it is false that Eugene is overweight.
         * Hubert, who is 19 years old, has a BMI of 50.0 and it is true that Hubert is overweight.
         * Abby, who is 23 years old, has a BMI of 17.0 and it is false that Abby is overweight.
         * Eduardo, who is 35 years old, has a BMI of 25.0 and it is true that Eduardo is overweight.

         * 
         */
        
    }
}