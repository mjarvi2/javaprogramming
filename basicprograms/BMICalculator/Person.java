// Person class
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Person
{
    private String name;
    private int age;
    private HeightAndWeight height_weight;
    public Person(String name, int age, HeightAndWeight height_weight){
        this.name = name;
        this.age = age;
        this.height_weight = height_weight;
    }
    public String getName(){
     return name;   
    }
    public int getAge(){
     return age;   
    }
    public double BMI(){
        double bmi = (Math.round(height_weight.getWeight()/
                (Math.pow(height_weight.getHeight(),2))* 100))/100;
        return bmi;   
    }
    public boolean isOverweight(){
        return BMI() > 24.9;
    }
    public String toString(){
        return(name + ", who is "+ age + " years old, has a BMI of " + BMI() + 
                " and it is "+isOverweight()+ " that "+name+" is overweight." );  
    }
}