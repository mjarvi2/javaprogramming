public class IntList{
    IntNode head;
    boolean insert_as_head = true;
    public void insert(int new_value, int before_value){
        IntNode temp1 = head;
        IntNode temp2 = null;
        while(temp1 != null && temp1.getValue() != before_value){
            temp2 = temp1;
            temp1 = temp1.getNext();
            insert_as_head = false;
        }
        if(insert_as_head){
            head = new IntNode(new_value, temp2);
        }
        else{
            temp2.setNext(new IntNode(new_value, temp1));
        }
    }
    public boolean isEmpty(){
     return head == null;   
    }
    public void append(int num){
        IntNode temp = head;
        if(isEmpty()){
            head = new IntNode(num, null);
        }
        else{
           while(temp.getNext() != null){
               temp = temp.getNext();
           }
           temp.setNext(new IntNode(num, null)); 
        }
    }
    public void remove(int beforeNum){
        boolean deleteHead = true;
        IntNode temp1 = head;
        IntNode temp2 = head;
        while(temp1.getValue() != beforeNum){
            temp2 = temp1;
            temp1 = temp1.getNext();
            deleteHead = false;
        }
        if(deleteHead) head = head.getNext();
        else
            temp2.setNext(temp1.getNext());
    }
    public void print(){ 
        IntNode current = head;       
        while (current != null){
            System.out.println(current.getValue());
            current = current.next;
        }
    }
    public void reset(){
        head = null;
    }
}