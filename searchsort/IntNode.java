public class IntNode{
    private int value;
    IntNode next;
    public IntNode(int v, IntNode next){
        value = v;
        this.next = next;
    }
    public int getValue(){
     return value;   
    }
    public IntNode getNext(){
        return next;
    }
    public void setNext(IntNode next){
        this.next = next;
    }
}