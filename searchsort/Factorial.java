// Lab7
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Factorial
{
   public static void main(String[]args){
       
       System.out.print(fact(17));
       
            
   }
   public static int fact(int n){
           if(n<=0)
            return 1;
           return n * fact(n-1);
        }
        //largest number that can be computed correctly is 16 because 17 will cause a number
        //larger than an int can hold.
}
