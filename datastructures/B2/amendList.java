package B2;

//Michael Jarvis and John Mulholland
import java.util.Random;
public class amendList{
    //keep track of first element of first list
    private skipList head;
    private skipList currentList;
    private skipList nextList;
    private int dispCount = 0;
    private Random gen = new Random();
    public amendList(){
        //initialize vars. this will only be done once.
        head = new skipList();
        currentList= head;
        nextList = head.getNextList();
    }
    public void insert(int n){
        //pass insert to treeList
        head.insertValue(n);
    }
    public void delete(){
        //randomly do delete for full list
        head.resetCurrentNode();
        currentList.resetCurrentNode();
        numNode temp = currentList.passNode();
        currentList.initNextList();
        nextList = currentList.getNextList();
        double num;
        while(temp != null){
            num = gen.nextDouble();
            if(num>0.5){
                nextList.insertValue(Integer.MIN_VALUE);
            }
            else{
                nextList.insertValue(temp.value);
            }
            temp = temp.getNext();
        }
        currentList = currentList.getNextList();
        
    }
    public void displayAll(){
        //using the array to flip the output, couldn't think
        //of a way to do it in the loop without recursion or
        //it being extremely complex
        head.resetCurrentNode();
        currentList = head;
        String[] display = new String[10];
        for(int k = 0; k<10;k++){
            display[k] = "";
        }
        while(currentList.getNextList() != null){
            numNode num = currentList.passNode();
            while(num!= null){
                if(num.value > Integer.MIN_VALUE){
                    display[dispCount] = display[dispCount] + num.value + " ";
                }
                num = num.getNext();
            }
            currentList = currentList.getNextList();
            currentList.resetCurrentNode();
            dispCount++;
        }
        for(int i = dispCount-1; i>=0; i--){
            System.out.println(display[i]);
        }
    }
    public String search(int n){
        //similar loop to the display, just comparing searched number
        //to each element from each list, returning which rows it shows up in.
        String display = n + " appears in rows: ";
        boolean exists = false;
        int rowCount = 0;
        head.resetCurrentNode();
        currentList = head;
        while(currentList.getNextList() != null){
            numNode num = currentList.passNode();
            while(num!= null){
                if(num.value == n){
                    display = display + rowCount + ", ";
                    exists = true;
                }
                num = num.getNext();
            }
            currentList = currentList.getNextList();
            currentList.resetCurrentNode();
            rowCount++;
        }
        if(!exists) return n + " does not exist in this list.";
        return display;
    }
}
