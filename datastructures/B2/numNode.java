package B2;


public class numNode{
    int value;
    numNode nextValue;
    public numNode(int v, numNode n){
        value = v;
        nextValue = n;
    }
    public void setNext(int n){
        nextValue = new numNode(n, null);
    }
    public numNode getNext(){
        return nextValue;
    }
}
