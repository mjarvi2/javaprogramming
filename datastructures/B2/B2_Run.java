package B2;

//Michael Jarvis and John Mulholland
import java.io.*;
import java.util.regex.Pattern;
import java.util.Scanner;
public class B2_Run{
    //currently a bug when there is only 1 number left in the list
    //it will print multiple times
    private static final Pattern space = Pattern.compile(" ");
    private static amendList list1 = new amendList();
    public static void main(String[]args){
        Scanner console = new Scanner(System.in);
        try{
            init();
        }
        catch(IOException e){
           System.out.println("incorrect file"); 
        }
        for(int i = 0; i<10; i++){
            list1.delete();
        }
        boolean cont = true;
        int input = 0;
        list1.displayAll();
        while(cont){
            System.out.println("Enter number to search: ");
            input = console.nextInt();
            System.out.println(list1.search(input));
            System.out.println("Do you want to continue searching (y/n)");
            cont = console.next().equalsIgnoreCase("y");
        }
    }
    public static void init()throws IOException{
        try{
            BufferedReader in = new BufferedReader(new FileReader("B2/inputHW03B2.txt"));
            String line;
            while((line = in.readLine()) != null){
                for(String token: space.split(line)){
                    list1.insert(Integer.parseInt(token));
                }
            }
            in.close();
        }
        catch(FileNotFoundException e){
        System.err.println("file not found");
        }
    }
}
