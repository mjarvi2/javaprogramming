package B2;

public class skipList{
    numNode head;
    numNode currentNode;
    skipList nextList;
    public skipList(){
        head = new numNode(Integer.MIN_VALUE, null);
        currentNode = head;
        nextList = null;
    }
    public void insertValue(int n){
        currentNode.setNext(n);
        currentNode = currentNode.getNext();
    }
    public void initNextList(){
        nextList = new skipList();
    }
    public skipList getNextList(){
        return nextList;
    }
    public numNode passNode(){
        return currentNode;
    }
    public void nextNode(){
        currentNode = currentNode.getNext();
    }
    public void resetCurrentNode(){
        currentNode = head;
    }
}