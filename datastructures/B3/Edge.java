package B3;
//Michael Jarvis and John Mulholland


public class Edge{
    private double weight;
    private Point startPoint;
    private Point endPoint;
    public Edge(double w, Point s, Point e){
        weight = w;
        startPoint = s;
        endPoint = e;
    }
    public void setWeight(double w){
        weight = w;
    }
    public double getWeight(){
        return weight;
    }
    public void setStartPoint(Point p){
        startPoint = p;
    }
    public Point getStartPoint(){
        return startPoint;
    }
    public void setEndPoint(Point p){
        endPoint = p;
    }
    public Point getEndPoint(){
        return endPoint;
    }
}
