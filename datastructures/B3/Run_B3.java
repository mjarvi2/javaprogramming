package B3;
//Michael Jarvis and John Mulholland

public class Run_B3{
    //some parts of this program came from java2blog.com and a previous 
    //program I did in high school on triangles
    public static void main(String [] args){
        Point pointA = new Point("A");
        Point pointB = new Point("B");
        Point pointC = new Point("C");
        Point pointD = new Point("D");
        Point pointE = new Point("E");
        Point pointF = new Point("F");
        pointA.addNeighbor(new Edge(3, pointA, pointB));
        pointA.addNeighbor(new Edge(6, pointA, pointC));
        pointA.addNeighbor(new Edge(5, pointA, pointD));
        pointB.addNeighbor(new Edge(9, pointB, pointD));
        pointD.addNeighbor(new Edge(13, pointD, pointE));
        pointE.addNeighbor(new Edge(4, pointE, pointF));
        Dijkstra shortestPathA = new Dijkstra();
        Dijkstra shortestPathB = new Dijkstra();
        shortestPathA.findShortestPath(pointA);
        shortestPathB.findShortestPath(pointB);
        System.out.println("Minimum distance from A to B: "+pointB.getDistance());
	System.out.println("Minimum distance from A to C: "+pointC.getDistance());
	System.out.println("Minimum distance from A to D: "+pointD.getDistance());
	System.out.println("Shortest Path from A to E: "+shortestPathA.getShortestPathTo(pointE));
	System.out.println("Shortest Path from A to F "+shortestPathA.getShortestPathTo(pointF));
	System.out.println("Shortest Path from B to E: "+shortestPathB.getShortestPathTo(pointE));
	System.out.println("Shortest Path from B to F "+shortestPathB.getShortestPathTo(pointD));
    }
}
