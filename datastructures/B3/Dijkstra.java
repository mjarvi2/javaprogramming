package B3;
//Michael Jarvis and John Mulholland
import java.util.*;
//ideas for this algorithm came fromjava2blog.com
public class Dijkstra{
    public void findShortestPath(Point p){
        double distance = 0.0;
        p.setDistance(0.0);
        PriorityQueue<Point> queue = new PriorityQueue<>();
        queue.add(p);
        p.setVisited(true);
        while(!queue.isEmpty()){
            Point p1 = queue.poll();
            for(Edge edge : p.getAdjacenciesList()){
                Point p2 = edge.getEndPoint();
                if(!p2.getVisited()){
                    distance = p1.getDistance() + edge.getWeight();
                    if(distance < p2.getDistance()){
                        queue.remove(p2);
                        p2.setDistance(distance);
                        p2.setPrevious(p1);
                        queue.add(p2);
                    }
                }
            }
            p1.setVisited(true);
        }
    }
    public List<Point> getShortestPathTo(Point endPoint){
        List<Point> path = new ArrayList<>();
        for(Point p = endPoint; p != null; p = p.getPrevious()){
            path.add(p);
        }
        Collections.reverse(path); 
        return path;
    }
}
