package B3;
//Michael Jarvis and John Mulholland
import java.util.ArrayList;
import java.util.List;

public class Point implements Comparable<Point>{
    private String name;
    private List<Edge> adjacenciesList;
    private double distance = Double.MAX_VALUE;
    private boolean visited;
    private Point previous;
    public Point(String n){
        name = n;
        adjacenciesList = new ArrayList<>();
    }
    public void addNeighbor(Edge e){
        adjacenciesList.add(e);
    }
    public void setName(String n){
        name = n;
    }
    public String getName(){
        return name;
    }
    public void setAdjacenciesList(List<Edge> l){
        adjacenciesList = l;
    }
    public List<Edge> getAdjacenciesList(){
        return adjacenciesList;
    }
    public void setVisited(boolean b){
        visited = b;
    }
    public boolean getVisited(){
        return visited;
    }
    public void setPrevious(Point p){
        previous = p;
    }
    public Point getPrevious(){
        return previous;
    }
    public void setDistance(double d){
        distance = d;
    }
    public double getDistance(){
        return distance;
    }
    public String toString(){
        return name;
    }
    public int compareTo(Point otherPoint){
        return Double.compare(distance, otherPoint.getDistance());
    }
}
