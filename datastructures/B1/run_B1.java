package B1;

//Michael Jarvis and John Mulholland
import java.io.*;
import java.util.regex.Pattern;
import java.util.Scanner;
public class run_B1{
    private static final Pattern space = Pattern.compile(" ");
    public static void main(String[]args){
        Scanner console = new Scanner(System.in);
        boolean cont = true;
        int selection;
        int value;
        //the head of the tree in memory will be the smallest value an int 
        //can store so that the actual head of the tree is always to
        //the right of the head stored in memory
        treeList tree = new treeList(Integer.MIN_VALUE);
        //intialize and display tree for first time before asking user 
        //what to do with the tree
        try{
            init();
        }
        catch(IOException e){
            System.out.println("invalid input");
        }
        tree.display();
        while(cont){
            System.out.println("1 Insert");
            System.out.println("2 Search");
            System.out.println("3 count nodes");
            selection = console.nextInt();
            switch(selection){
                case 1:
                    System.out.println("Enter non-negative number");
                    value = console.nextInt();
                    if(value<0) System.out.println("invalid value");
                    else tree.insert(value);
                    break;
                case 2:
                    System.out.println("Enter non-negative number");
                    value = console.nextInt();
                    if(value<0) System.out.println("invalid value");
                    else System.out.println(tree.search(value));
                    break;
                case 3:
                    break;
                    
            }
            tree.display();
            System.out.println("continue (yes/no)");
                cont = !console.next().equalsIgnoreCase("no");
        }
    }
    public static void init()throws IOException{
        try{
            BufferedReader in = new BufferedReader(new FileReader("B1/inputHW03B1.txt"));
            String line;
            while((line = in.readLine()) != null){
                for(String token: space.split(line)){
                    try{
                        treeList.insert(Integer.parseInt(token));
                    }
                    catch(NumberFormatException ex){
                        System.err.println(token + " is not a number");
                    }
                }
            }
            in.close();
        }
        catch(FileNotFoundException e){
        System.err.println("file not found");
        }
    }
}
