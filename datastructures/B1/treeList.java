package B1;

//hw03 Michael Jarvis, John Mulholland
public class treeList{
    private static intNode head;
    private static intNode current;
    private static intNode parent;
    private static intNode grandparent;
    private static intNode greatgrandparent;
    //will be used to init children
    private static intNode nullNode;
    static{
        nullNode = new intNode(0, null, null);
        nullNode.leftChild = nullNode;
        nullNode.rightChild = nullNode;
    }
    public treeList(int num){
        head = new intNode(num);
        head.leftChild = nullNode;
        head.rightChild = nullNode;
    }
    public static void insert(int num){
        current = head;
        parent = head;
        grandparent = head;
        greatgrandparent = head;
        nullNode.value = num;
        while(current.value != num){
            greatgrandparent = grandparent; 
            grandparent = parent;
            parent = current;
            current = num < current.value ? current.leftChild : current.rightChild;
            if(current.leftChild.color == 'R' && current.rightChild.color == 'R'){
                reorient(num);
            }
        }
        if(current != nullNode){
            return;
        }
        current = new intNode(num, nullNode, nullNode);
        if(num < parent.value){
            parent.leftChild = current;
        }
        else{
            parent.rightChild = current;
        }
        reorient(num);
    }
    private static void reorient(int num){
        current.color = 'R';
        current.leftChild.color = 'B';
        current.rightChild.color = 'B';
        if(parent.color == 'R'){
            grandparent.color = 'R';
            if(num< grandparent.value != num < parent.value){
                parent = rotate(num, grandparent);
            }
            current = rotate(num, greatgrandparent);
            current.color = 'B';
        }
        head.rightChild.color = 'B';
    }
    private static intNode rotate(int num, intNode parent){
        if(num < parent.value){
            return parent.leftChild = num < parent.leftChild.value ? rotateWithLeftChild(parent.leftChild) : rotateWithRightChild(parent.leftChild);
        }
        return parent.rightChild = num < parent.rightChild.value ? rotateWithLeftChild(parent.rightChild) : rotateWithRightChild(parent.rightChild);
    }
    private static intNode rotateWithLeftChild(intNode temp1){
        intNode temp2 = temp1.leftChild;
        temp1.leftChild = temp2.rightChild;
        temp2.rightChild = temp1;
        return temp2;
    }
    private static intNode rotateWithRightChild(intNode temp1){
        intNode temp2 = temp1.rightChild;
        temp1.rightChild = temp2.leftChild;
        temp2.leftChild = temp1;
        return temp2;
    }
    public boolean search(int val){
        //assume binary tree is correctly sorted into red-black tree
        boolean found = false;
        intNode current = head.rightChild;
        int currentVal;
        while((current != nullNode) && !found){
            currentVal = current.value;
            if(val > currentVal){
                current = current.leftChild;
            }
            else if(val > currentVal){
                current = current.leftChild;
            }
            else{
                return !found;
            }    
        }
        return found;
    }
    public void display(){
        //this will be able to display trees with up to 7 levels
        intNode [] currentParents = new intNode[128];
        intNode [] currentChildren = new intNode[128];
        int j = 0; //to iterate throguh list
        currentParents[0] = head.rightChild;
        System.out.println("("  + valWithSign(currentParents[0]) +  ", null)");
        //while(count > 0){
        int k = 0;
        for(int z = 0; z<7; z++){
            for(k = 0; k < countElements(currentParents); k++){
                currentChildren[j] = currentParents[k].leftChild;
                if(currentChildren[j] != nullNode){
                    System.out.print("(" + valWithSign(currentChildren[j]) + ", " +  valWithSign(currentParents[k]) + ")");
                }
                j++;
                currentChildren[j] = currentParents[k].rightChild;
                if(currentChildren[j] != nullNode){
                    System.out.print("(" + valWithSign(currentChildren[j]) + ", " +  valWithSign(currentParents[k]) + ")");
                }
                j++;
            }
            for(int p = 0; p < 64; p++){
                currentParents[p] = null;
            }
            for(int n = 0; n < 64; n++){
                currentParents[n] = currentChildren[n];
            }
            for(int m = 0; m < 64; m++){
                currentChildren[m] = null;
            }
            j=0;
            System.out.println();
        }
    }
    public int countElements(intNode[] array){
        int count = 0;
        for(int i = 0; i<64; i++){
            if(array[i] != null) count++;
        }
        return count;
    }
    private String valWithSign(intNode node){
        if(node.color == 'R') return "-" + node.value;
        return node.value + "";
    }
}
