package B1;

//hw03 Michael Jarvis, John Mulholland
public class intNode{
    //using protected access to more easily access the children
    int value;
    intNode leftChild;
    intNode rightChild;
    char color;
    public intNode(int val, intNode left, intNode right){
        value = val;
        leftChild = left;
        rightChild = right;
        color = 'B';
    }
    public intNode(int val){
        value = val;
        leftChild = null;
        rightChild = null;
    }
}
