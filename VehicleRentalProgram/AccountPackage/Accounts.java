package AccountPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
import java.util.ArrayList;
public class Accounts{
    private Account accounts;
    private int current, count;
    public Accounts(){
        accounts = null;
        count = 0;
        current = 0;
    }
    public int getCount(){
        return count;
    }
    public boolean isEmpty(){
        return accounts == null;   
    }
    public void add(Account acct){
        Account temp = accounts;
        for(int i = 0; i < acct.getAcctNum().length(); i++){
            if(!Character.isDigit(acct.getAcctNum().charAt(i))){
                throw new InvalidAcctNumException();
            }
        }
        if(acct.getAcctNum().length() != 5){
            throw new InvalidAcctNumException();
        }
        else if(isEmpty()){
            count++;
            accounts = new Account(acct);
        }
        else{
           while(temp.getNext() != null){
               temp = temp.getNext();
           }
           temp.setNext(new Account(acct)); 
           count++;
        }
    }
    public Account getAccount(String acct) throws AccountNumberNotFoundException{
            Account temp = accounts; 
                while(temp!=null){
                    if(temp.getAcctNum().equals(acct)){
                     return temp;   
                    }
                    temp = temp.getNext();
                }
                throw new AccountNumberNotFoundException();
    }
    public boolean hasNext(){
        return count>current;
    }
    public Account getNext(){
        Account a = accounts;
        for(int i = 0; i< current; i++){
            a = a.getNext();
        }
        current++;
        return a;
    }
    public void reset(){
        current = 0;
    }
}
