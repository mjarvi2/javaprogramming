package AccountPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Account{
    private String acct_num;
    private String company_name;
    private boolean prime_customer;
    private Account next;
    public Account(String acct_num, String company_name, boolean prime_customer, Account next){
         this.acct_num = acct_num;
         this.company_name = company_name;
         this.prime_customer = prime_customer;
         this.next = next;
    }
    public Account(Account other){
         this.acct_num = other.acct_num;
         this.company_name = other.company_name;
         this.prime_customer = other.prime_customer;
         this.next = other.next;
    }
    public String getAcctNum(){
        return acct_num;
    }
    public String getAcctName(){
        return company_name;
    }
    public boolean primeCustomer(){
     return prime_customer;  
    }
    public Account getNext(){
        return next;
    }
    public void setNext(Account next){
        this.next = next;
    }
    public String toString(){
        return "Company name: " + company_name + " Account number: " + acct_num + 
        " Prime customer: " + prime_customer;
    }
}
