package RatePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Cost implements CostType{
    double dailyRate, weeklyRate, monthlyRate, mileageChrg, insurChrg;
   public Cost(double dailyRate, double weeklyRate, double monthlyRate, double mileageChrg,
        double insurChrg){
        this.dailyRate = dailyRate;
        this.weeklyRate = weeklyRate;
        this.monthlyRate = monthlyRate;
        this.mileageChrg = mileageChrg;
        this.insurChrg = insurChrg;
   }
   public double getDailyRate(){
       return dailyRate;
   }
   public double getWeeklyRate(){
       return weeklyRate;
   }
   public double getMonthlyRate(){
       return monthlyRate;
   }
   public double getMileageChrg(){
       return mileageChrg;
   }
   public double getDailyInsurRate(){
       return insurChrg;
   }
}
