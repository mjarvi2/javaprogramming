package RatePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public interface CostType{
    public double getDailyRate();
    public double getWeeklyRate();
    public double getMonthlyRate();
    public double getMileageChrg();
    public double getDailyInsurRate();
}
