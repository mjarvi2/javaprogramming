package RatePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public abstract class VehicleRates{
    double daily_rate, weekly_rate, monthly_rate, mileage_chrg, daily_insur_rate;
    public VehicleRates(double daily_rate, double weekly_rate, double monthly_rate,
        double mileage_chrg, double daily_insur_rate){
            this.daily_rate = daily_rate;
            this.weekly_rate = weekly_rate;
            this.monthly_rate = monthly_rate;
            this.mileage_chrg = mileage_chrg;
            this.daily_insur_rate = daily_insur_rate;
    }
    public double getDailyRate(){
         return daily_rate;
    }
    public double getWeeklyRate(){
        return weekly_rate;
    }
    public double getMonthlyRate(){
        return monthly_rate;
    }
    public double getMileageChrg(){
        return mileage_chrg;
    }
    public double getDailyInsurRate(){
        return daily_insur_rate;
    }
    public abstract String toString();
    public Cost cloneAsCostType(){
        return new Cost(getDailyRate(), getWeeklyRate(), getMonthlyRate(), getMileageChrg(),
           getDailyInsurRate());
    }
}
