package RatePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Rates{
    private VehicleRates[] rates = new VehicleRates[3];
    CarRates carRate = null;
    SUVRates suvRate = null;
    TruckRates truckRate = null;
    public Rates(CarRates carRate, SUVRates suvRate, TruckRates truckRate){
        rates[0] = carRate;
        rates[1] = suvRate;
        rates[2] = truckRate;
    }
    public VehicleRates getCarRates(){
        return rates[0];
    }
    public void setCarRates(CarRates cr){
        rates[0] = cr;
    }
    public VehicleRates getSUVRates(){
        return rates[1];
    }
    public void setSUVRates(SUVRates sr){
        rates[1] = sr;
    }
    public VehicleRates getTruckRates(){
        return rates[2];
    }
    public void setTruckRates(TruckRates tr){
        rates[2] = tr;
    }
    public double calcEstimatedCost(int vehicleType, String estimatedRentalPeriod, 
        int estimatedNumMiles, boolean dailyInsur, boolean primeCustomer){
        double totalCost = 0;
        int dayMultiplier = 0;
        switch(vehicleType){
            case 1:
                if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("d")){
                    dayMultiplier = 1;
                    totalCost = totalCost + rates[0].getDailyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                else if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("w")){
                    dayMultiplier = 7;
                    totalCost = totalCost + rates[0].getWeeklyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                else if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("m")){
                    dayMultiplier = 30;
                    totalCost = totalCost + rates[0].getMonthlyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                if(primeCustomer){
                    totalCost = totalCost + rates[0].getMileageChrg() * estimatedNumMiles
                        -100;
                }
                else if(!primeCustomer){
                    totalCost = totalCost + rates[0].getMileageChrg() * estimatedNumMiles;
                }
                if(dailyInsur){
                    totalCost = totalCost + rates[0].getDailyInsurRate() * dayMultiplier *
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
            break;
            case 2:
                if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("d")){
                    dayMultiplier = 1;
                    totalCost = totalCost + rates[1].getDailyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                else if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("w")){
                    dayMultiplier = 7;
                    totalCost = totalCost + rates[1].getWeeklyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                else if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("m")){
                    dayMultiplier = 30;
                    totalCost = totalCost + rates[1].getMonthlyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                if(primeCustomer){
                    totalCost = totalCost + rates[1].getMileageChrg() * estimatedNumMiles
                        -100;
                }
                else if(!primeCustomer){
                    totalCost = totalCost + rates[1].getMileageChrg() * estimatedNumMiles;
                }
                if(dailyInsur){
                    totalCost = totalCost + rates[1].getDailyInsurRate() * dayMultiplier *
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
            break;
            case 3:
                if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("d")){
                    dayMultiplier = 1;
                    totalCost = totalCost + rates[1].getDailyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                else if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("w")){
                    dayMultiplier = 7;
                    totalCost = totalCost + rates[1].getWeeklyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                else if(estimatedRentalPeriod.substring(0,1).equalsIgnoreCase("m")){
                    dayMultiplier = 30;
                    totalCost = totalCost + rates[1].getMonthlyRate() * 
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
                if(primeCustomer){
                    totalCost = totalCost + rates[1].getMileageChrg() * estimatedNumMiles
                        -100;
                }
                else if(!primeCustomer){
                    totalCost = totalCost + rates[1].getMileageChrg() * estimatedNumMiles;
                }
                if(dailyInsur){
                    totalCost = totalCost + rates[1].getDailyInsurRate() * dayMultiplier *
                        Integer.parseInt(estimatedRentalPeriod.substring(1,2));
                }
           break;
        }
        return totalCost;
    }
}

