package RatePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class SUVRates extends VehicleRates{
    public SUVRates(double daily_rate, double weekly_rate, double monthly_rate,
        double mileage_chrg, double daily_insur_rate){
            super(daily_rate, weekly_rate, monthly_rate,mileage_chrg, daily_insur_rate);
    }
    public String toString(){
        return "daily rate: " + daily_rate + " weekly rate: " + weekly_rate + " monthly rate: "+
            monthly_rate + " mileage charge: " + mileage_chrg + " daily insurance rate: " + 
                daily_insur_rate + "\n";
    }  
    public Cost getClonedCostType(){
     return new Cost(getDailyRate(), getWeeklyRate(), getMonthlyRate(), getMileageChrg(),
        getDailyInsurRate());
    }
}
