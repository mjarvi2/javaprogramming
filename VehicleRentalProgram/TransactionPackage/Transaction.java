package TransactionPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Transaction extends Transactions{
    private String acct_num;
    private String company_name;
    private int vehicle_type;
    private String rental_period;
    private String rental_cost;
    public Transaction(String acct_num, String company_name, int vehicle_type,
        String rental_period, String rental_cost){
            this.acct_num = acct_num;
            this.company_name = company_name;
            this.vehicle_type = vehicle_type;
            this.rental_period = rental_period;
            this.rental_cost = rental_cost;
    }
    public String toString(){
        return "Company name: " + company_name + " Account number: " + acct_num +
            " Vehicle type: " + vehicle_type + " Rental period: " + rental_period +
            " Rental cost: " + rental_cost;
    }
}
