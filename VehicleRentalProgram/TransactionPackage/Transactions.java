package TransactionPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
import java.util.ArrayList;

public class Transactions{
    private ArrayList<Transaction> transactions;
    private int count;
    public Transactions(){
        transactions = new ArrayList<Transaction>();
        count = 0;
    }
    public void add(Transaction tran){
        transactions.add(tran);
    }
    public boolean hasNext(){
        return count+1 <= transactions.size();
    }
    public int getSize(){
        return transactions.size();
    }
    public Transaction getNext(){
        count++;
        return transactions.get(count-1);
    }
    public void reset(){
        count = 0;
    }
}
