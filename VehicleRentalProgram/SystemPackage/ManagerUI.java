package SystemPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
import java.util.Scanner;
import java.util.Random;
import RatePackage.*;
import java.util.ArrayList;
public class ManagerUI implements UserInterface{
    private ArrayList<String> generatedNums = new ArrayList();
    public void start() {
        int selection;
        Scanner input = new Scanner(System.in);
        boolean quit = false;
        
        // command loop
        while(!quit) {
            displayMenu();
            selection = getSelection(input);
            if(selection == 7){
                quit = true;
            }
            execute(selection, input, quit);
        }   
    }    
    private void execute(int selection, Scanner input, boolean quit) {
        int veh_type;
        String vin, acct_num;
        String[] display_lines = null;
        RentalDetails rental_details; 
        ReservationDetails reserv_details;
        switch(selection){
            case 1:
                System.out.print("Car Rates: ");
                displayResults(SystemInterface.getCarRates());
                System.out.print("SUV Rates: ");
                displayResults(SystemInterface.getSUVRates());
                System.out.print("Truck Rates: ");
                displayResults(SystemInterface.getTruckRates());
                System.out.println("1 - Change Car Rates \n2 - Change SUV Rates \n3 -" + 
                    " Change Truck Rates \n4 - no change");
                    int changeR = input.nextInt();
                    switch(changeR){
                        case 1:
                            display_lines = SystemInterface.updateCarRates(new CarRates(
                                getDailyRate(input), getWeeklyRate(input),
                                getMonthlyRate(input), getMileageChrg(input),
                                getInsurRate(input)));
                                displayResults(display_lines);
                        break;
                        case 2:
                            display_lines = SystemInterface.updateSUVRates(new SUVRates(
                                getDailyRate(input), getWeeklyRate(input),
                                getMonthlyRate(input), getMileageChrg(input),
                                getInsurRate(input)));
                                displayResults(display_lines);
                        break;
                        case 3:
                            display_lines = SystemInterface.updateTruckRates(new TruckRates(
                                getDailyRate(input), getWeeklyRate(input),
                                getMonthlyRate(input), getMileageChrg(input),
                                getInsurRate(input)));
                                displayResults(display_lines);
                        break;
                    }
            break;
            case 2:
                System.out.println("All vehicles: ");
                displayResults(SystemInterface.getAllVehicles()); 
            break;
            case 3:
                System.out.println("What is the compnay name?");
                String cName = input.next();
                boolean primeCust = false;
                System.out.println("prime customer? (true/false)");
                if(input.next().equalsIgnoreCase("true"))
                    primeCust = true;
                display_lines = SystemInterface.addAccount(generateAcctNumber(), cName,
                    primeCust);
                displayResults(display_lines);
            break;
            case 4:
                System.out.println("All Reservations:");
                displayResults(SystemInterface.getAllReservations());
            break;
            case 5:
                System.out.println("All Accounts:");
                displayResults(SystemInterface.getAllAccounts());
            break;
            case 6:
                System.out.println("All Transactions:");
                displayResults(SystemInterface.getAllTransactions());
            break;
            case 7: quit = true;
        } 
    }
    private void displayMenu(){  
        System.out.println("Main Menu - Manager\n");
        System.out.println("1 - View/Update Rates");
        System.out.println("2 - View Vehicles");
        System.out.println("3 - Add Account");
        System.out.println("4 - View All Reservations");
        System.out.println("5 - View All Accounts");
        System.out.println("6 - View All Transactions");
        System.out.println("7 - Quit");
    }
    private String generateAcctNumber(){
        //returns new acct number and stores it in a list for checking multiple of same num
        Random rand = new Random();
        String acctNum = "";
        boolean exit = false;
        String temp = "";
        while(!exit){
            acctNum = rand.nextInt(10) + "" + rand.nextInt(10) + "" +rand.nextInt(10) + "" +
            rand.nextInt(10) + "" + rand.nextInt(10) + "";
            for(int i=0; i<generatedNums.size(); i++){
                if(acctNum.equals(generatedNums.get(i))){
                    break; 
                }
                temp = generatedNums.get(i);
                generatedNums.add(acctNum);
            }
            if(!acctNum.equals(temp)){
                exit = true;
            }
        }
        return acctNum + "";
    }
    private double getDailyRate(Scanner input){
        System.out.println("What is the new daily rate?");
        return input.nextDouble();
    }
    private double getWeeklyRate(Scanner input){
        System.out.println("What is the new weekly rate?");
        return input.nextDouble();
    }
    private double getMonthlyRate(Scanner input){
        System.out.println("What is the new monthly rate?");
        return input.nextDouble();
    }
    private double getMileageChrg(Scanner input){
        System.out.println("What is the new mileage charge?");
        return input.nextDouble();
    }
    private double getInsurRate(Scanner input){
        System.out.println("What is the new insurance rate?");
        return input.nextDouble();
    }
    // displays the menu of options
    private int getSelection(Scanner input) {  
        int i = 0;
        while(i<1 || i>7){
            System.out.println("Make a Selection from the menu");
            i = input.nextInt();
        }
        return i;
    }
    private void displayResults(String[] lines){
        for(int i = 0; i< lines.length; i++){
            System.out.println(lines[i].toString());
        }
        System.out.println();
    }
}
