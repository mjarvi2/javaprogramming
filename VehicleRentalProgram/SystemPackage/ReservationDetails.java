package SystemPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class ReservationDetails{
    String VIN;
    String acct_num;
    String rental_period;
    boolean daily_insur;
    public ReservationDetails(String VIN, String acct_num, String rental_period, boolean daily_insur){
        this.VIN = VIN;
        this.acct_num = acct_num;
        this.rental_period = rental_period;
        this.daily_insur = daily_insur;
    }
    public String getVIN(){
        return VIN;
    }
    public String getAcctNum(){
        return acct_num;
    }
    public String getRentalPeriod(){
        return rental_period;
    }
    public boolean dailyInsurSelected(){
        return daily_insur;
    }
}
