package SystemPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class RentalDetails{
    int vehType;
    String rentalPeriod;
    int estMiles;
    boolean insurSelected;
    boolean primeCustomer;
    public RentalDetails(int vehType, String rentalPeriod, int estMiles, boolean insurSelected,
        boolean primeCustomer){
        this.vehType = vehType;
        this.rentalPeriod = rentalPeriod;
        this.estMiles = estMiles;
        this.insurSelected = insurSelected;
        this.primeCustomer = primeCustomer;
    }
    public int getVehicleType(){
        return vehType;
    }
    public String getRentalPeriod(){
        return rentalPeriod;
    }
    public int getEstimatedMiles(){
        return estMiles;
    }
    public boolean getInsuranceSelected(){
        return insurSelected;
    }
    public boolean getPrimeCustomer(){
        return primeCustomer;
    }
}

