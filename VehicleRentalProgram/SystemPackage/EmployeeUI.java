package SystemPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
import java.util.Scanner;

public class EmployeeUI implements UserInterface{
    // no constructor needed, calls static methods of the SystemInterface
    // starts a “command loop” that repeatedly: (a) displays a menu of options, (b) gets the selected
    // option from the user, and (c) executes the corresponding command.
    boolean quit;
    public void start() {
        int selection = 0;
        Scanner input = new Scanner(System.in);
        quit = false;
        // command loop
        while(!quit) {
            displayMenu();
            selection = getSelection(input);
            if(selection == 8){
                quit = true;
            }
            execute(selection, input, quit);
        }   
    }    
    private void execute(int selection, Scanner input, boolean quit) {
        int veh_type;
        String vin, acct_num;
        String[] display_lines = null;
        RentalDetails rental_details; 
        ReservationDetails reserv_details;
        switch(selection) {
            // display rental rates
            case 1: 
                veh_type = getVehicleType(input);
                switch(veh_type){
                    case 1: display_lines = SystemInterface.getCarRates(); break;
                    case 2: display_lines = SystemInterface.getSUVRates(); break;
                    case 3: display_lines = SystemInterface.getTruckRates(); break;
                }
                displayResults(display_lines);
            break;

            // display available vehicles
            case 2: 
                veh_type = getVehicleType(input);
                switch(veh_type){
                    case 1: display_lines = SystemInterface.getAvailCars(); break;
                    case 2: display_lines = SystemInterface.getAvailSUVs(); break;
                    case 3: display_lines = SystemInterface.getAvailTrucks(); break;
                }
                displayResults(display_lines);
            break;

            // display estimated rental cost
            case 3:
                rental_details = getRentalDetails(input);
                display_lines = SystemInterface.estimatedRentalCost(rental_details);
                displayResults(display_lines);
            break;
            // make a reservation
            case 4:
                reserv_details = getReservationDetails(input);
                display_lines = SystemInterface.makeReservation(reserv_details);
                displayResults(display_lines);
                break;
        
            // cancel a reservation
            case 5:
                vin = getVIN(input);
                display_lines = SystemInterface.cancelReservation(vin);
                displayResults(display_lines);
                break;

            // view corporate account (and company reservations)
            case 6:
                acct_num = getAcctNumber(input);
                display_lines = SystemInterface.getAccount(acct_num);
                displayResults(display_lines);
                displayResults(SystemInterface.getAllReservations(acct_num));
                break;

            // process returned vehicle
            case 7:
             acct_num = getAcctNumber(input);
                vin = getVIN(input);
                System.out.println("enter number of days used");
                int num_day_used = input.nextInt();
                System.out.println("Enter number of miles driven");
                int num_miles_driven = input.nextInt();
                display_lines = SystemInterface.processReturnedVehicle(vin, 
                                       num_day_used,num_miles_driven);
                displayResults(display_lines);
                break;
    }
    // ------- private methods
    }
    private void displayMenu(){  
        System.out.println("Main Menu - Employee\n");
        System.out.println("1 - View Current Rates");
        System.out.println("2 - View Available Vehicles");
        System.out.println("3 - Calc Estimated Rental Cost");
        System.out.println("4 - Make a Reservation");
        System.out.println("5 - Cancel a Reservation");
        System.out.println("6 - View Corporate Account");
        System.out.println("7 - Process Returned Vehicle");
        System.out.println("8 - Quit");
    }
    // displays the menu of options
    private int getSelection(Scanner input) {  
        int i = 0;
        while(i<1 || i>8){
            System.out.println("Make a Selection from the menu");
            i = input.nextInt();
        }
        return i;
    }
    // prompts user for selection from menu (continues to prompt is selection < 1 or selection > 8)
    private String getAcctNumber(Scanner input){
        String s = "";
        while(s.length()!=5){
            System.out.println("What is the account number");
            s = input.next();
        }
        return s;
    }
    // generates and returns 5 digit account number 
    private String getVIN(Scanner input){
        System.out.println("Enter the VIN number");
        return input.next();
    }
    // prompts user to enter VIN for a given vehicle (does not do any error checking on the input)
    private int getVehicleType(Scanner input){
        int i = 0;
        while(i<1 || i>3){
            System.out.println("Enter vehicle type: 1-car, 2-SUV, 3-Truck");
            i = input.nextInt();
        }
        return i;
    }
    // prompts user to enter 1, 2, or 3, and returns (continues to prompt user if invalid input given) 
    private RentalDetails getRentalDetails(Scanner input){
        String vehType = "";
        String rentalPeriod = "";
        boolean insur = false;
        boolean primeCust = false;
        int estMiles = 0;
        System.out.println("Enter vehicle type: 1-car, 2-SUV, 3-Truck");
        vehType = input.next();
        System.out.println("Enter rental period (ex: d1, w3, m2)");
        rentalPeriod = input.next();
        System.out.println("Select insurance? (true/false)");
        if(input.nextLine().equalsIgnoreCase("true"))
            insur = true;
        System.out.println("Enter estimated number of miles");
        estMiles = input.nextInt();
        System.out.println("Is a prime customer (true/false)");
        if(input.next().equalsIgnoreCase("true"))
            primeCust = true;
        return new RentalDetails(Integer.parseInt(vehType), rentalPeriod, estMiles, insur, primeCust);
    }
    // prompts user to enter required information for an estimated rental cost (vehicle type, estimated  
    // number of miles expected to be driven, rental period (number of days, weeks or months), and
    // insurance option, returning the result packaged as a RentalDetails object (to be passed in method
    // calls to the SystemInterface)

    private ReservationDetails getReservationDetails(Scanner input){
        String vin = "";
        String acctNum = "";
        String rentalPeriod = "";
        boolean insur = false;
        System.out.println("Enter the vin number of the vehicle you want to reserve");
        vin = input.next();
        System.out.println("Enter the account number");
        acctNum = input.next();
        System.out.println("Enter the rental period");
        rentalPeriod = input.next();
        System.out.println("Insurace selected (true/false)");
        if(input.next().equalsIgnoreCase("true"))
            insur = true;
            System.out.println(vin + " " + acctNum + " " + rentalPeriod + " " + insur);
        return new ReservationDetails(vin, acctNum, rentalPeriod, insur);
    }
    // prompts user to enter required information for making a reservation (VIN of vehicle to reserve, 
    // acct number, rental period, and insurance option), returning the result packaged as a 
    // ReservationDetails object (to be passed in method calls to the SystemInterface)

    private void displayResults(String[] lines){
        for(int i = 0; i< lines.length; i++){
            System.out.println(lines[i].toString());
        }
        System.out.println();
    }
    // displays the array of strings passed, one string per screen line
}
