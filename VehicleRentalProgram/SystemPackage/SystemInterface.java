package SystemPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
import RatePackage.Rates;
import RatePackage.CarRates;
import RatePackage.SUVRates;
import RatePackage.TruckRates;
import VehiclePackage.Vehicles;
import VehiclePackage.*;
import AccountPackage.*;
import TransactionPackage.*;
class SystemInterface {
    private static Rates agency_rates;
    private static Vehicles vehicleList = new Vehicles();
    private static Accounts acct = new Accounts();
    private static Transactions transactions_history;
    static boolean initialized = false;
    public static boolean initialized(){
        return initialized;
    }
    public static void initSystem(){
        createVehicles();
        createRates();
        createTransactions();
        initialized = true;
    }
    public static void createVehicles(){
        //creates all of the vehicles
        vehicleList.add(new Car("Cevrolet Camaro-2018", 30,"HK4GM4565GD", 2));
        vehicleList.add(new Car("Ford Fusion- 2019", 34, "AB4FG5689GM", 4));
        vehicleList.add(new Car("Ford Fusion Hybrid-2017", 32, "KU4EG3245RW", 4));
        vehicleList.add(new Car("Chevrolet Impala-2019", 20, "RK3BM4256YH", 4));
        vehicleList.add(new SUV("Honda Odyssey-2020", 28, "VM9RF2635TD", 7 , 6));
        vehicleList.add(new SUV("Dodge Caravan- 2019", 25, "QK3FL4273ME", 5, 4));
        vehicleList.add(new SUV("Ford Expedition- 2018", 20, "JK2RT9364HY", 5, 3));
        vehicleList.add(new Truck("Ten-Foot", 12, "EJ5KU2435BC", 2810));
        vehicleList.add(new Truck("Seventeen-Foot", 10, "KG4DM5472RK", 5930));
        vehicleList.add(new Truck("Twenty-Four-Foot", 8, "EB2WR3082QB", 6500));
        vehicleList.add(new Truck("Twenty-Four-Foot", 8, "TW3GH4290EK", 6500));
    }
    public static Rates createRates(){
        //creates rates for each type of vehicle
        CarRates carRate = new CarRates(24.99, 99.99, 299.99, .05, 7);
        SUVRates suvRate = new SUVRates(29.99, 119.99, 349.99, .07, 9);
        TruckRates truckRate = new TruckRates(34.99, 149.99, 399.99, .09, 12);
        agency_rates = new Rates(carRate, suvRate, truckRate);
        return agency_rates;
    } 
    public static Transactions createTransactions(){
        //create transactions for companies
        transactions_history = new Transactions();
        return transactions_history;
    }
    // vehicle rates-related methods
    public static String[ ] getCarRates() { 
        //returns car rates toString method
        String[] s = new String[1];
        s[0] = agency_rates.getCarRates().toString();
        return s;
    }
    public static String[ ] getSUVRates() { 
        //returns suv rates toString method
        String[] s = new String[1];
        s[0] = agency_rates.getSUVRates().toString();
        return s;
    }
    public static String[ ] getTruckRates() { 
        //returns truck rates toString method
        String[] s = new String[1];
        s[0] = agency_rates.getTruckRates().toString();
        return s;
    }
    public static String[ ] updateCarRates(CarRates r) { 
        //update car rates
        String[] s = new String[1];
        s[0] = "successfully changed rate\n";
        agency_rates.setCarRates(r);
        return s;
    }
    public static String[ ] updateSUVRates(SUVRates r) { 
        //update suv rates
        String[] s = new String[1];
        s[0] = "successfully changed rate\n";
        agency_rates.setSUVRates(r);
        return s;
    }
    public static String[ ] updateTruckRates(TruckRates r) { 
        //update truck rates
        String[] s = new String[1];
        s[0] = "successfully changed rate\n";
        agency_rates.setTruckRates(r);
        return s;
    }
    public static String[ ] estimatedRentalCost(RentalDetails details) { 
        String[]s = new String[1];
        int vehType = details.getVehicleType();
        String rentalPeriod = details.getRentalPeriod();
        int miles = details.getEstimatedMiles();
        boolean insur = details.getInsuranceSelected();
        boolean prime = details.getPrimeCustomer();
        s[0] = "Estimate: " +agency_rates.calcEstimatedCost(vehType, rentalPeriod, miles,
            insur, prime);
        return s;
    }    
    public static String[ ] processReturnedVehicle(String vin, int num_days_used,
        int num_miles_driven){ 
        String[] s = new String[2];
        Vehicle v = vehicleList.getVehicle(vin);
        String acctNum = v.getReservation().getAcctNum();
        String acctName = acct.getAccount(acctNum).getAcctName();
        boolean primeCust = acct.getAccount(acctNum).primeCustomer();
        int vehType = v.getReservation().getVehicleType();
        double mileageChrg = v.getCost().getMileageCharge();
        int months = 0;
        int weeks = 0;
        int days = 0;
        int numDays = num_days_used;
        double cost = 0;
        while(num_days_used >29){
            months++;
            num_days_used = num_days_used - 30;
        }
        while(num_days_used > 6){
            weeks++;
            num_days_used = num_days_used - 7;
        }
        while(num_days_used > 0){
            days++;
            num_days_used--;
        }
        if(primeCust && num_miles_driven > 100){
             cost = cost + (mileageChrg * (num_miles_driven -100));
        }
        else if(!primeCust){
            cost = cost + (mileageChrg * num_miles_driven);
        }
        cost = cost + v.getCost().getMonthlyRate() * months;
        cost = cost + v.getCost().getWeeklyRate() * weeks;
        cost = cost + v.getCost().getDailyRate() * days;
        if(v.getReservation().getInsuranceSelected()){
            cost = cost + v.getCost().getDailyInsuranceRate() * numDays;
        }
        s[1] = "Cost - $" + cost;
        transactions_history.add(new Transaction(acctNum, acctName, vehType, "d" + 
            numDays, cost + ""));
        v.cancelReservation();
        v.setCost(null);
        s[0] = "Successfully returned vehicle.\n";
        return s;
    }
    // vehicle-related methods
    public static String[ ] getAvailCars() { 
        int num = 0;
        vehicleList.reset();
        while(vehicleList.hasNext()){
            Vehicle v = vehicleList.getNext();
            if(v.getReservation()==null && v.getVehicleType() == 1)
                num++;
        }
        String [] display = new String[num];
        vehicleList.reset();
        int i = 0;
        while(vehicleList.hasNext()){
            Vehicle temp = vehicleList.getNext();
            if(temp.getVehicleType() == 1 && temp.getReservation()==null){
                display[i] = temp.toString();
                i++;
            }
        }
        return display;
    }
    public static String[ ] getAvailSUVs() { 
        int num = 0;
        vehicleList.reset();
        while(vehicleList.hasNext()){
            Vehicle v = vehicleList.getNext();
            if(v.getVehicleType() == 2 && v.getReservation()==null)
                num++;
        }
        String [] display = new String[num];
        vehicleList.reset();
        int i = 0;
        while(vehicleList.hasNext()){
            Vehicle temp = vehicleList.getNext();
            if(temp.getVehicleType() == 2 && temp.getReservation()==null){
                display[i] = temp.toString();
                i++;
            }
        }
        return display;
    }
    public static String[ ] getAvailTrucks() { 
        int num = 0;
        vehicleList.reset();
        while(vehicleList.hasNext()){
            Vehicle v = vehicleList.getNext();
            if(v.getVehicleType() == 3 && v.getReservation()==null)
                num++;
        }
        String [] display = new String[num];
        vehicleList.reset();
        int i = 0;
        while(vehicleList.hasNext()){
            Vehicle temp = vehicleList.getNext();
            if(temp.getVehicleType() == 3 && temp.getReservation()==null){
                display[i] = temp.toString();
                i++;
            }
        }
        return display;
    }
    public static String[ ] getAllVehicles() { 
        int num = 0;
        vehicleList.reset();
        while(vehicleList.hasNext()){
            Vehicle v = vehicleList.getNext();
            num++;
        }
        String [] display = new String[num];
        vehicleList.reset();
        int i = 0;
        while(vehicleList.hasNext()){
            display[i] = vehicleList.getNext().toString();
            i++;
        }
        return display;
    }
    
    public static String[ ] makeReservation(ReservationDetails details){ 
            Vehicle veh = vehicleList.getVehicle(details.getVIN());
            String[]s = new String[1];
            try{
                veh.setReservation(new Reservation(details.getAcctNum(),vehicleList.getVehicle(details.getVIN()).getVehicleType(),
                    vehicleList.getVehicle(details.getVIN()).getDescript(), details.getRentalPeriod(),details.dailyInsurSelected()));
                
                if(veh.getVehicleType() ==1){
                    veh.setCost(new Cost(agency_rates.getCarRates().cloneAsCostType().getDailyRate(),
                        agency_rates.getCarRates().cloneAsCostType().getWeeklyRate(),
                        agency_rates.getCarRates().cloneAsCostType().getMonthlyRate(),
                        agency_rates.getCarRates().cloneAsCostType().getMileageChrg(),
                        agency_rates.getCarRates().cloneAsCostType().getDailyInsurRate()));
                }
                else if(veh.getVehicleType() ==2){
                    veh.setCost(new Cost(agency_rates.getSUVRates().cloneAsCostType().getDailyRate(),
                        agency_rates.getSUVRates().cloneAsCostType().getWeeklyRate(),
                        agency_rates.getSUVRates().cloneAsCostType().getMonthlyRate(),
                        agency_rates.getSUVRates().cloneAsCostType().getMileageChrg(),
                        agency_rates.getSUVRates().cloneAsCostType().getDailyInsurRate()));
                }
                else{
                    veh.setCost(new Cost(agency_rates.getTruckRates().cloneAsCostType().getDailyRate(),
                        agency_rates.getTruckRates().cloneAsCostType().getWeeklyRate(),
                        agency_rates.getTruckRates().cloneAsCostType().getMonthlyRate(),
                        agency_rates.getTruckRates().cloneAsCostType().getMileageChrg(),
                        agency_rates.getTruckRates().cloneAsCostType().getDailyInsurRate()));
                }
                s[0] = "Successfully made reservation";
            }
            catch(ReservedVehicleException e){
                s[0] = "This vehicle is already reserved";
            }
            catch(VINNotFoundException e){
                s[0] = "This vehicle does not exist";
            }
            return s;
    }
    
    public static String[ ] cancelReservation(String vin) { 
        String[] s = new String[1];
        Vehicle v = vehicleList.getVehicle(vin);
        try{
            v.cancelReservation();
            v.setCost(null);
            s[0] = "Successfully canceled reservation\n";
        }
        catch(UnreservedVehicleException e){
            s[0] = "This vehicle is not reserved";
        }
        return s;
    }    
    public static String[ ] getReservation(String vin) { 
        String[]s = new String[1];
        try{
            Vehicle v = vehicleList.getVehicle(vin);
            s[0] = v.getReservation().toString();
        }
        catch(UnreservedVehicleException e){
            s[0] = "Vehicle is not reserved";
        }
        return s;
    }
    public static String[ ] getAllReservations() { 
        int num = 0;
        vehicleList.reset();
        while(vehicleList.hasNext()){
            Vehicle v = vehicleList.getNext();
            if(v.getReservation()!=null)
                num++;
        }
        String [] display = new String[num];
        vehicleList.reset();
        int i = 0;
        while(vehicleList.hasNext()){
            Vehicle temp = vehicleList.getNext();
            if(temp.getReservation()!=null){
                display[i] = temp.getReservation().toString() + "\n";
                i++;
            }
        }
        return display;
    }
    public static String[] getAllReservations(String acctNum){
        int num = 0;
        vehicleList.reset();
        while(vehicleList.hasNext()){
            Vehicle v = vehicleList.getNext();
            if(v.getReservation()!=null && v.getReservation().getAcctNum().equals(acctNum))
                num++;
        }
        String [] display = new String[num];
        vehicleList.reset();
        int i = 0;
        while(vehicleList.hasNext()){
            Vehicle temp = vehicleList.getNext();
            if(temp.getReservation()!=null && temp.getReservation().getAcctNum().equals(acctNum)){
                display[i] = temp.getReservation().toString() + "\n";
                i++;
            }
        }
        return display;
    }
    // customer accounts-related methods
    public static String[ ] addAccount(String acct_num, String company_name, 
        boolean prime_cust) { 
        String[]s = new String[1];
        acct.add(new Account(acct_num, company_name,prime_cust, null));
        s[0] = "successfully added account, the account number is " + acct_num;
        return s;
    }
    public static String[ ] getAccount(String acct_num) { 
        String[]s = new String[1];
        try{
            s[0] = acct.getAccount(acct_num).toString();
        }
        catch(AccountNumberNotFoundException e){
            s[0] = "Account not found";
        }
        return s;
    }
    
    public static String[ ] getAllAccounts() { 
        String[]s = new String[acct.getCount()];
        for(int i = 0; i<acct.getCount(); i++){
            s[i] = acct.getNext().toString();
        }
        return s;
    }
    // transactions-related methods
    public static String[ ] getAllTransactions() {
        String []s = new String[transactions_history.getSize()];
        int i = 0;
        while(transactions_history.hasNext()){
            s[i] = transactions_history.getNext().toString();
            i++;
        }
        return s;
    }
}
