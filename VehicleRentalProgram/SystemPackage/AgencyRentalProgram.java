package SystemPackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
import RatePackage.*;
import VehiclePackage.*;
import TransactionPackage.*;
import AccountPackage.*;
import java.util.Scanner;
public class AgencyRentalProgram {
    static UserInterface ui;
    public static void main(String[ ] args) {
        // Establish User Interface
        Scanner input = new Scanner(System.in);
        boolean quit = false;
        SystemInterface.initSystem();
        // Create Requested UI and Begin Execution 
        while(!quit) {  // (allows switching between Employee and Manager user interfaces while running)
            ui = getUI(input);
            if(ui == null)
                quit = true;
            else {
                // Init System Interface with Agency Data (if not already initialized)
                if(!SystemInterface.initialized())
                      SystemInterface.initSystem();
                // Start User Interface
                ui.start();
            }
        }
    }
    public static UserInterface getUI(Scanner input) {
        
        boolean valid_selection = false;
        int selection;
        while(!valid_selection) {
            System.out.print("1 - Employee, 2 - Manager, 3 - Quit");
            selection = input.nextInt();
            if(selection == 1) {
                ui = new EmployeeUI();
                valid_selection = true;
            }
            else if(selection == 2) {
                ui = new ManagerUI();
                valid_selection = true;
            }
            else if(selection == 3) {
                ui = null;
                valid_selection = true;
            }
            else
                System.out.println("Invalid Selection - Please Re-Enter");
        }
        return ui;
    }
}
