package VehiclePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class SUV extends Vehicle
{
   private int seats, cargo;
   public SUV(String description, int mpg, String vin, int seats, int cargo){
       super(description, mpg, vin, 2);
       this.seats = seats;
       this.cargo = cargo;
   }
   public String toString(){
       
       return "Description: " + getDescript() + " mpg: " + getMPG()  + " seats: " + seats +
       " cargo: " + cargo + " cubic feet."  + " vin: " + getVIN() + "\n";
   }
}
