package VehiclePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Truck extends Vehicle{
    private int load;
   public Truck(String description, int mpg, String vin, int load){
       super(description, mpg, vin, 3);
       this.load = load;
   }
   public String toString(){
   return "Description: " + getDescript() + " mpg: " + getMPG()  + " load capacity: " + load
        + " vin: " +getVIN() + "\n";
   }
}