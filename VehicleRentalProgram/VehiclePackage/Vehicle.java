package VehiclePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
import RatePackage.CostType;
import AccountPackage.InvalidAcctNumException;
public abstract class Vehicle{
    private String description;
    private int mpg;
    private String vin;
    private Reservation resv;
    private Cost rates;
    private int vehicleType;
    public Vehicle(String description, int mpg, String vin, int type){
        this.description = description;
        this.mpg = mpg;
        this.vin = vin;
        resv = null;
        rates = null;
        vehicleType = type;
    }
    public String getDescript(){
     return description;   
    }
    public int getVehicleType(){
        return vehicleType;
    }
    public int getMPG(){
        return mpg;
    }
    public String getVIN(){
        return vin;
    }
    public Reservation getReservation(){
        return resv;
    }
    public Cost getCost(){
        return rates;   
    }
    public boolean isReserved(){
        return resv != null;
    }
    public abstract String toString();
    public void setReservation(Reservation resv)throws ReservedVehicleException, 
        InvalidAcctNumException{
        if(!isReserved()){
            this.resv = resv;
        }
        else if(isReserved()){
             throw new ReservedVehicleException();
        }
        else{
            throw new InvalidAcctNumException();
        }
    }
    public void setCost(Cost rates){
        this.rates = rates;
    }
    public void cancelReservation() throws UnreservedVehicleException{
        if(resv == null){
            throw new UnreservedVehicleException();
        }
        else{
            resv = null;
        }
    }
}
