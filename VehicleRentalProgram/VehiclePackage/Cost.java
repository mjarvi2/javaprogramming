package VehiclePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Cost {
    //THE NAME OF THIS CLASS SHOULD BE "Cost", SAFEASSIGN WILL NOT ALLOW ME TO SUBMIT
    //2 FILES OF THE SAME NAME.
    double daily_rate, weekly_rate, monthly_rate, mileage_chrg, daily_insur_rate;
    public Cost(double daily_rate, double weekly_rate, double monthly_rate,
        double mileage_chrg, double daily_insur_rate){
            this.daily_rate = daily_rate;
            this.weekly_rate = weekly_rate;
            this.monthly_rate = monthly_rate;
            this.mileage_chrg = mileage_chrg;
            this.daily_insur_rate = daily_insur_rate;
    }
    public double getDailyRate(){
        return daily_rate;
    }
    public double getWeeklyRate(){
        return weekly_rate;
    }
    public double getMonthlyRate(){
        return monthly_rate;
    }
    public double getMileageCharge(){
        return mileage_chrg;
    }
    public double getDailyInsuranceRate(){
        return daily_insur_rate;
    }
}

