package VehiclePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Reservation{
    private String acctNum;
    private int vehicleType;
    private String description;
    private String rentalPeriod;
    private boolean insuranceSelected;
    public Reservation(String acctNum, int vehicleType, String description,
        String rentalPeriod, boolean insuranceSelected){
            this.acctNum = acctNum;
            this.vehicleType = vehicleType;
            this.description = description;
            this.rentalPeriod = rentalPeriod;
            this.insuranceSelected = insuranceSelected;
    }
    public String getAcctNum(){
         return acctNum;
    }
    public int getVehicleType(){
        return vehicleType;
    }
    public String getDescript(){
        return description;
    }
    public String getRentalPeriod(){
        return rentalPeriod;
    }
    public boolean getInsuranceSelected(){
        return insuranceSelected;
    }
    public String toString(){
     return "vehicle: " + description + " account number " + acctNum + " rental period: " + rentalPeriod +
        " insurance selected? " + insuranceSelected;
    }
}
