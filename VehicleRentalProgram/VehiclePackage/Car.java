package VehiclePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
public class Car extends Vehicle
{
   private int seats;
   public Car(String description, int mpg, String vin, int seats){
       super(description, mpg, vin, 1);
       this.seats = seats;
   }

   public String toString(){
       return "Description: " + getDescript() + " mpg: " + getMPG()  + " seats: " + seats 
        + " vin: " +getVIN() + "\n";        
   }
}
