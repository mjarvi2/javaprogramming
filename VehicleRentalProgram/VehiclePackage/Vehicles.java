package VehiclePackage;
// Rental Agency Program
// Michael Jarvis
// COSC 237-003
// Fall 2019
import java.util.ArrayList;
public class Vehicles{
    private ArrayList<Vehicle> agency_vehicles;
    private int current;
    public Vehicles(){
        agency_vehicles = new ArrayList<Vehicle>();
        current = 0;
    }
    public void add(Vehicle v){
        agency_vehicles.add(v);
    }
    public void remove(String VIN)throws VINNotFoundException{
        try{
            for(int i = 0; i<agency_vehicles.size(); i++){
                    if(agency_vehicles.get(i).getVIN().equals(VIN)){
                        agency_vehicles.remove(agency_vehicles.get(i));
                    }
                }
        }
        catch(VINNotFoundException e){
            throw new VINNotFoundException();
        }
    }
    public Vehicle getVehicle(String VIN) throws VINNotFoundException{
        Vehicle v = null;
        try{
            for(int i = 0; i<agency_vehicles.size(); i++){
                if(agency_vehicles.get(i).getVIN().equals(VIN)){
                 v = agency_vehicles.get(i);   
                }
            }
        }
        catch(VINNotFoundException e){
            throw new VINNotFoundException();
        }
        return v;
    }
    public void reset(){
     current = 0;   
    }
    public boolean hasNext(){
        return current != agency_vehicles.size();
    }
    public Vehicle getNext(){
        if(hasNext()){
            current++;
            return agency_vehicles.get(current -1);
        }
        return null;
    }
}
