public class Recursion2{
    public static void main(String []args){
        int []a = {1, 4, 6};
        int []b = {2, 3, 5, 6, 7};
        merge(a, b);
        
    }
    public static int[] merge(int []A, int []B){
       int []merged = new int[A.length + B.length];
       int a = 0;
       int b = 0;
       for(int i = 0; i< merged.length; i++){
         if(a >= A.length){
             merged[i] = B[b];
             System.out.print(merged[i]);
             b++;
             continue;
         }
         if(b >= B.length){
             merged[i] = A[a];
             System.out.print(merged[i]);
             a++;
             continue;
         }
         if(A[a] >= B[b]){
             merged[i] = B[b];   
             System.out.print(merged[i]);
             b++;
         }
         else{
             merged[i] = A[a];
             System.out.print(merged[i]);
             a++;
         }
        }
        return merged;
    }
}