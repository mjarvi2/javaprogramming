class BinarySearch { 
    int binarySearch(int arr[], int x, int y, int z) 
    { 
        if (y >= x) { 
            int mid = x + (y - x) / 2; 
  
            if (arr[mid] == z) 
                return mid; 
 
            if (arr[mid] > z) 
                return binarySearch(arr, x, mid - x, z); 
            return binarySearch(arr, mid + 1, y, z); 
        }  
        return -1; 
    } 
public static void main(String args[]) 
    { 
        BinarySearch ob = new BinarySearch(); 
        int arr[] = { 2, 3, 4, 10, 40 }; 
        int n = arr.length; 
        int x = 10; 
        int result = ob.binarySearch(arr, 0, n - 1, x); 
        if (result == -1) 
            System.out.println("Element not present"); 
        else
            System.out.println("Element found at index " + result); 
    } 
} 