
public class Tester
{
    public static void main(String[]args){
        CoordinateList coors = new CoordinateList();
        coors.append(new Coordinate(2,2));
        coors.append(new Coordinate(3,4));
        coors.append(new Coordinate(5,3));
        coors.print();
        coors.remove(3);
        coors.changeY(2, 2, 6);
        coors.print();
    }
}
