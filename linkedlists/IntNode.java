
public class IntNode{
    private int i;
    private IntNode next;
    public IntNode(int i, IntNode next){
        this.i = i;
        this.next = next;
    }
    public int getInt(){
        return i;
    }
    public void setInt(int i){
        this.i = i;
    }
    public IntNode getNext(){
        return next;
    }
    public void setNext(IntNode next){
        this.next = next;
    }
}
