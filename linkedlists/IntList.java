

public class IntList{
    private IntNode head;
    public IntList(){
        head = null;
    }
    public boolean isEmpty(){
        return head==null;
    }
    public boolean hasNext(){
        return head.getNext()!=null;
    }
    public void append(int i){
        IntNode temp = head;
        if(isEmpty()){
            head = new IntNode(i, null);
        }
        else{
            while(temp.getNext() !=null){
                temp = temp.getNext();
            }
            temp.setNext(new IntNode(i, null));
        }
    }
    public void remove(int i){
        boolean removeHead = true;
        IntNode temp1 = head;
        IntNode temp2 = head;
        while(temp1.getInt()!=i){
            temp2 = temp1;
            temp1 = temp1.getNext();
            removeHead = false;
        }
        if(removeHead){
            head = head.getNext();
        }
        else{
            temp2.setNext(temp1.getNext());
        }
    }
    public void changeInt(int oldInt, int newInt){
        IntNode temp = head;
        while(temp != null){
            if(temp.getInt() == oldInt){
                temp.setInt(newInt);
            }
            temp = temp.getNext();
        }
    }
    public void print(){
        IntNode temp = head;
        while(temp!=null){
            System.out.print(temp.getInt() + ", ");
            temp = temp.getNext();
            
    
        }
    }
}
