
public class Roomate{
    private int room;
    private String name1, name2;
    public Roomate(int room, String name1, String name2){
        this.room = room;
        this.name1 = name1;
        this.name2 = name2;
    }
    public int getRoom(){
        return room;
    }
    public void setRoom(int room){
        this.room = room;
    }
    public String getName1(){
        return name1;
    }
    public void setName1(String name){
        name1 = name;
    }
    public String getName2(){
        return name2;
    }
    public void setName2(String name){
        name2 = name;
    }
}
