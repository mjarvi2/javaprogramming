
public class CoordinateList{
    private CoordinateNode coor;
    public CoordinateList(){
        coor = null;
    }
    public boolean isEmpty(){
        return coor==null;
    }
    public boolean hasNext(){
        return coor.getNext()!=null;
    }
    public void append(Coordinate newCoor){
        CoordinateNode temp = coor;
        if(isEmpty()){
            coor = new CoordinateNode(newCoor , null);
        }
        else{
            while(temp.getNext()!=null){
                temp = temp.getNext();
            }
            temp.setNext(new CoordinateNode(newCoor, null));
        }
    }
    public void remove(int x){
        boolean deleteHead = true;
        CoordinateNode temp1 = coor;
        CoordinateNode temp2 = coor;
        while(temp1.getCoordinate().getX() != x){
            temp2 = temp1;
            temp1 = temp1.getNext();
            deleteHead = false;
        }
        if(deleteHead) coor = coor.getNext();
        else 
            temp2.setNext(temp1.getNext());
    }
    public void changeY(int x, int oldY, int newY){
        CoordinateNode temp = coor;
        while(temp!=null){
            if(temp.getCoordinate().getX() == x && temp.getCoordinate().getY() == oldY){
                temp.getCoordinate().setY(newY);
            }
            temp = temp.getNext();
        }
    }
    public void print(){
        CoordinateNode temp = coor;
        while(temp != null){
            System.out.println(temp.getCoordinate().getX() + " , " + 
                temp.getCoordinate().getY());
            temp = temp.getNext();
        }
        System.out.println();
    }
}
