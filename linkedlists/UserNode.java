
public class UserNode{
    private User head;
    private UserNode next;
    public UserNode(User head, UserNode next){
        this.head = head;
        this.next = next;
    }
    public UserNode getNext(){
        return next;
    }
    public void setNext(UserNode next){
        this.next = next;
    }
    public User getUser(){
        return head;
    }
    public void setUser(User head){
        this.head = head;
    }
}
