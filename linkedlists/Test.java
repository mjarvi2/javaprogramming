
public class Test{
    public static void main(String[]args){
        UserAccounts users = new UserAccounts();
        users.addUser(new User("name1", "user1", "pass1"));
        users.addUser(new User("name2", "user2", "pass2"));
        users.addUser(new User("name3", "user3", "pass3"));
        users.addUser(new User("name4", "user4", "pass4"));
        users.printUsers();
        System.out.println();
        users.deleteUser("user3");
        users.printUsers();
        System.out.println();
        System.out.println(users.findPassword("user3"));
        users.printUsers();
        System.out.println();
        users.changePassword("user1", "pass1", "tiddies");
        users.printUsers();
    }
}
