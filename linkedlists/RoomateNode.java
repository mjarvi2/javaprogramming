
public class RoomateNode{
    private Roomate room;
    private RoomateNode next;
    public RoomateNode(Roomate room, RoomateNode next){
        this.room = room;
        this.next = next;
    }
    public Roomate getRoom(){
        return room;
    }
    public void setRoom(Roomate room){
        this.room = room;
    }
    public RoomateNode getNext(){
        return next;
    }
    public void setNext(RoomateNode next){
        this.next = next;
    }
}
