
public class CoordinateNode{
   private Coordinate coor;
   private CoordinateNode next;
   public CoordinateNode(Coordinate coor, CoordinateNode next){
       this.coor = coor;
       this.next = next;
   }
   public CoordinateNode getNext(){
       return next;
   }
   public void setNext(CoordinateNode next){
       this.next = next;
   }
   public Coordinate getCoordinate(){
       return coor;
   }
   public void setCoordinate(Coordinate coor){
       this.coor = coor;  
   }
}
