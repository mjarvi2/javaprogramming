
public class RoomateList{
    RoomateNode room;
    public RoomateList(){
        room = null;
    }
    public boolean isEmpty(){
        return room == null;
    }
    public boolean hasNext(){
        return room.getNext() != null;
    }
    public void append(Roomate room){
        RoomateNode temp = this.room;
        if(isEmpty()){
            this.room = new RoomateNode(room, null);
        }
        else{
            while(temp.getNext()!= null){
                temp = temp.getNext();
            }
            temp.setNext(new RoomateNode(room,null));
        }
    }
    public void remove(int roomNum){
        boolean removeRoom = true;
        RoomateNode temp1 = room;
        RoomateNode temp2 = room;
        while(temp1.getRoom().getRoom()!= roomNum){
            temp2 = temp1;
            temp1 = temp1.getNext();
            removeRoom = false;
        }
        if(removeRoom){
            room = room.getNext();
        }
        else{
            temp2.setNext(temp1.getNext());
        }
    }
    public void changeRoomNum(int oldNum, int newNum){
        RoomateNode temp = room;
        while(temp.getRoom().getRoom()!=oldNum){
            temp = temp.getNext();
        }
        temp.getRoom().setRoom(newNum);
    }
    public void print(){
        RoomateNode temp = room;
        while(temp!=null){
            System.out.println("Room number: " + temp.getRoom().getRoom() +
                " Roomate 1: " + temp.getRoom().getName1() + " Roomate 2: " + 
                    temp.getRoom().getName2());
            temp = temp.getNext();
        }
    }
}
