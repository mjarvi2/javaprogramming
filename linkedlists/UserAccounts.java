
public class UserAccounts{
    private UserNode accounts;
    public UserAccounts(){
        accounts = null;
    }
    public boolean isEmpty(){
        return accounts == null;
    }
    public String findPassword(String username){
        UserNode temp = accounts;
        while(temp != null){
            if(temp.getUser().getUsername().equals(username)){
                return temp.getUser().getPassword();
            }
            temp = temp.getNext();
        }
        return "User does not exist";
    }
    public void changePassword(String username, String oldPassword, String newPassword){
        UserNode temp = accounts;
        while(temp != null){
            if(temp.getUser().getUsername().equals(username)){
                if(temp.getUser().getPassword().equals(oldPassword)){
                    temp.getUser().setPassword(newPassword);
                }
            }
            temp = temp.getNext();
        }
    }
    public void addUser(User user){
        UserNode temp = accounts;
        if(isEmpty()){
            accounts = new UserNode(user, null);
        }
        else{
           while(temp.getNext() != null){
               temp = temp.getNext();
           }
           temp.setNext(new UserNode(user, null)); 
        }
    }
    public void deleteUser(String username){
        boolean deleteHead = true;
        UserNode temp1 = accounts;
        UserNode temp2 = accounts;
        while(temp1.getUser().getUsername() != username){
            temp2 = temp1;
            temp1 = temp1.getNext();
            deleteHead = false;
        }
        if(deleteHead) accounts = accounts.getNext();
        else
            temp2.setNext(temp1.getNext());
    }
    public void printUsers(){
        UserNode temp = accounts;
        while(temp != null){
            System.out.println(temp.getUser()); 
            temp = temp.getNext();
        }
    }
}
